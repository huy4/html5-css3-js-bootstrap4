# Introduction

# HTML5 CSS3 JS Bootstrap 4

### Certificate

![Certificate](./Certificate.jpg)

### Planning  

- Expected Time: 60h ( 50h flow with course and 10h practices)
- Finish day: 23/08/2018

----------------------


### Day 1: 12/08/2018

#### Today's progress:

- Finish Section 1: Overview course Build 3 Real World Websites Using HTML5, CSS3, JS, Bootstraps 4.
- Finish Section 2: Setup editor.
- Finish Section 3: Learning how to use Photoshop in slice websites

#### Throughts: 

- I am interesting this course

#### Link(s) to work:

1. [Section 1](./Chapter-1-Introduction+Course-Essentials/README.md#section1)
2. [Section 2](./Chapter-1-Introduction+Course-Essentials/README.md#section2)
3. [Section 3](./Chapter-1-Introduction+Course-Essentials/README.md#section3)

### Day 2: 13/08/2018

#### Today's progress:

- Finish Section 4: Learning use Bootstrap 4

#### Throughts:

- Using Bootstrap save time code
- Responsive very cool
- You can use css and instead of utility class but these classes buy you some time and reduce the amount of css that your run.

#### Link(s) to work:

1. [Section 4](./Chapter-1-Introduction+Course-Essentials/README.md#section4)

### Day 3: 14/08/2018

#### Today's progress:

- Finish Section 5: Learning Perprocessors Sass and Less
    - DRY principle (don't repeat yourself)
        - Variables
        - Maxins
        - Functions
    - Maintainability
    - Readability
    - Natural Extension

- Finish Section 6: Trello The Project Manager
    - Trello keeps track of everything
- Finish Section 7: OpenCharity Overview
    - Overview require of a website
- Finish Section 8: Prepare File
    - Step-by-step prepare file for project
    - Using Trello to keep track work
    - Make structure project's folder
- Finish Section 9: Linking File + Implementing Style sheet
    - Link file and font
    - Making some variables for color and font

#### Thoughts:

- Using SASS and Less is save code
- Should see general before code
- Keep track the process while working
- How to use Photoshop export image and font
- Using less helps to better reuse code 

#### Link(s) to work:

1. [Section 5](./Chapter-1-Introduction+Course-Essentials/README.md#section5)
2. [Section 6](./Chapter-1-Introduction+Course-Essentials/README.md#section6)
3. [Section 7](./Chapter-2-OpenCharity-Overview/README.md#section7)
4. [Section 8](./Chapter-2-OpenCharity-Overview/README.md#section8)
5. [Section 9](./Chapter-2-OpenCharity-Overview/README.md#section9)

### Day 4: 15/08/2018

#### Today's progress:

- Finish Section 10: Nav Bar - Part 1
    - Implement HTML for navbar
- Finish Section 11: Nav Bar - Part 2
    - Using jQuery make animation when scroll
- Finish Section 12: Header - Part 1
    - Implement HTML for header
    - It consists of two main parts
- Finish Section 13: Header - Part 2
    - Implement CSS to header
- Finish Section 14: Get Involved section
    - Implement UI for Get Involved section

#### Throughts:

#### Link(s) to work:

1. [Section 10](/Chapter-2-OpenCharity-Overview/README.md#section10)
2. [Section 11](/Chapter-2-OpenCharity-Overview/README.md#section11)
3. [Section 12](/Chapter-2-OpenCharity-Overview/README.md#section12)
4. [Section 13](/Chapter-2-OpenCharity-Overview/README.md#section13)
5. [Section 14](/Chapter-2-OpenCharity-Overview/README.md#section14)

### Day 5: 16/08/2018

#### Today's progress:

- Finish Section 15: Mission section
- Finish Section 16: Member section
- Finish Section 17: Event section
- Finish Section 18: Blog section
- Finish Section 19: Contact section and Footer

#### Throughts:

- Bootstrap Carousel is old slider and can't be easily customized.

#### Link(s) to work:

1. [Section 15](/Chapter-2-OpenCharity-Overview/README.md#section15)
2. [Section 16](/Chapter-2-OpenCharity-Overview/README.md#section16)
3. [Section 17](/Chapter-2-OpenCharity-Overview/README.md#section17)
4. [Section 18](/Chapter-2-OpenCharity-Overview/README.md#section18)
5. [Section 19](/Chapter-2-OpenCharity-Overview/README.md#section19)

### Day 6: 17/08/2018

#### Today's progress:

- Finish Section 20: Enhancements
    - Animate.css library
- Finish Section 21: Testing
    - Testing performance website, bug, optimize file
- Finish Section 22: Repo
    - Overview how to make OpenCharity project
- Finish Section 23: 404 Overview

- Finish Section 24: 404 Prepare File

#### Throughts:

- Test website is importain
- Optimize the picture helps performance very fast

#### Link(s) to work:

1. [Section 20](/Chapter-2-OpenCharity-Overview/README.md#section20)
2. [Section 21](/Chapter-2-OpenCharity-Overview/README.md#section21)
3. [Section 22](/Chapter-2-OpenCharity-Overview/README.md#section22)
4. [Section 23](/Chapter-3-404-penguins-page#section23)
5. [Section 24](/Chapter-3-404-penguins-page#section24)

### Day 7: 18/08/2018

#### Today's progress:

- Finish Section 25: 404 Start Code
    - Implement UI
    - Style page
    - Add animation
- Finish Section 26: Recap
    - A few other ways for 404 page
- Finish Section 27: Merkury Overview
    - Overview how to make Merkury project
- Finish Section 28: Prepare Files
    - Apply Trello steps
- Finish Section 29: Linking Files
    - define variables for font and color in style.less
    - use bootstrap 4, popper.js, jquery.js in project
- Finish Section 30: Navbar Part 1
    - Implement UI nav bar
    - Make nav bar by template nav bar in bootstrap 4 component
- Finish Section 31: Navbar Part 2
    - Implement animation for UI navbar by jquery

#### Throughts:

- Using `z-index`, `@keyframes`, `position` to this page.
- The jQuery API is great
- Make animation for page by `@keyframes`

#### Link(s) to work:

1. [Section 25](/Chapter-3-404-penguins-page#section25)
2. [Section 26](/Chapter-3-404-penguins-page#section26)
3. [Section 27](/Chapter-4-Merkury-Website#section27)
4. [Section 28](/Chapter-4-Merkury-Website#section28)
5. [Section 29](/Chapter-4-Merkury-Website#section29)
6. [Section 30](/Chapter-4-Merkury-Website#section30)
7. [Section 31](/Chapter-4-Merkury-Website#section31)

### Day 8: 19/08/2018

#### Today's progress:

- Finish Section 32: Header
    - Implement UI for header
- Finish Section 33: Services Section
    - Implement UI for services section
- Finish Section 34: Features Section
    - Implement UI for features section
- Finish Section 35: Drag and Drop Section part 1
    - Implement IO for drag and drop section
- Finish Section 36: Drag and Drop Section part 2
    - Improved UI for drag and drop section.
    - Using jQuery UI create action for task.

#### Throughts:

- `@keyframes` make animation easy
- jQuery ui makes dragging and dropping simple.

#### Link(s) to work:

1. [Section 32](/Chapter-4-Merkury-Website#section32)
2. [Section 33](/Chapter-4-Merkury-Website#section33)
3. [Section 34](/Chapter-4-Merkury-Website#section34)
4. [Section 35](/Chapter-4-Merkury-Website#section35)
5. [Section 36](/Chapter-4-Merkury-Website#section36)

### Day 9: 20/08/2018

#### Today's progress:

- Finish Section 37: Template Section
    - Implement UI for Template section
- Finish Section 38: Pricing Section
    - Implement UI for Pricing section
- Finish Section 39: Join - Section + Footer
    - Implement UI for Join section and Footer

#### Throughts:

- Remind of `::before` `display: inline` `display: flex`

#### Link(s) to work:

1. [Section 37](/Chapter-4-Merkury-Website#section37)
2. [Section 38](/Chapter-4-Merkury-Website#section38)
3. [Section 39](/Chapter-4-Merkury-Website#section39)

### Day 10: 21/08/2018

#### Today's progress:

- Finish Section 40: Enhancements
    - Fix bug UI in small devices
    - Effects when crolling pages
- Finish Section 41: Testing
    - Compressing images.
    - Cross browser testing.
    - static sites host.
    - W3C markup validation server.
    - Website speed test.
    - Deloy website in biballoon.com
- Finish Section 42: Recap
    - Summarizes the code Merkury project process.
- Finish Section 43: Important Tips
    - Have three directions after learning this course

#### Throughts:

- CSS effect is quite interesting
- I was test build and deploy, directly from my GitLab repository.

#### Link(s) to work:

1. [Section 40](/Chapter-4-Merkury-Website#section40)
2. [Section 41](/Chapter-4-Merkury-Website#section41)
3. [Section 42](/Chapter-4-Merkury-Website#section42)
4. [Section 43](/Chapter-4-Merkury-Website#section43)

### Day 11: 26/08/2018

#### Today's progress:

- Folder Structure include:
    - assets: 
        - img
        - font 
    - styles
    - script
- Finish html before code css.
- Using extension 'pixelperfect' or code to check distance elements.
  
#### Throughts:

- HTML CSS is not as simple as i throught.

#### Link(s) to work:

### Day 12: 27/08/2018

#### Today's progress:

- Create folder structure for project
- Learn more about the functionality of each folder

#### Throughts:

- Should detail the tasks of each file in each folder

Link(s) to work

- [Day 1](./important/README.md#section-1)

### Day 13: 06/09/2018

#### Today's progress:

- Apply pixelpefect to merkury page
- Learn more about HTML CSS

#### Throughts:

- Understand the use of margin and padding in CSS
- Not confuse class name
- The order of restricting the use of z-index tags

#### Link(s) to work:

- [Day 2](./important/README.md#section-2)

### Day 14: 12/09/2018

#### Today's progress:

- Visual regression test
- Production build

#### Throughts:

- Visual regression test:
    - Apply regression test and its importance in frontend development
    - [Jest](https://jestjs.io/) to test all Javascript
    - [Selenium WebDriver](https://www.seleniumhq.org/projects/webdriver/)
    - [Puppeteer](https://github.com/GoogleChrome/puppeteer)
    - [PhantomJS](http://phantomjs.org/)
    - After the test, if there is a difference, there are two options:
        - Reject code if too much deviation from the template
        - Accepted to meet according to code logic
- Production build
    - Have two enviroment:
        - Staging:
            - Dev and localhost:
                - Fast to build
                - Debugge able 

        - Production: 

                - Fast to run
                - Because: minify HTML, CSS, JS and obfucate JS
                - CSS: Critical/ above the fold, remove used CSS

#### Link(s) to work:

- none

### Day 15: 03/10/2018

#### Today's progress:

- Integrate surge deployment with gitlab

#### Throughts:
- Following name convention is very important 
- Tips & Tricks:
    - Problem: Change uppercase and lowercase git do not change
    - Solution: `git config core.ignorecase false`

#### Link(s) to work:

- [Merkury Dashboard](http://serious-degree.surge.sh/pages/home.html)

### Day 16: 04/10/2018

#### Today's progress:
- gulp
- webpack

#### Throughts:
- Vì sao cần gulp và webpack:
	- Trước đây công nghệ Server side render sử dụng HTML, CSS, JS một cách cồng kềnh. Khiến Code base khá nặng.
	- Nhu cầu cần thiết là module hóa các file
- NodeJS ra đời
	- Sơ lược: NodeJS được viết bằng c
	- Mở ra xu hướng task runner có [Grunt](https://gruntjs.com/) theo mô hình [Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) và Package Manager bao gồm npm và Bower.
	- Việc sử dụng công nghệ trên vẫn **chậm** do thao tác công việc vẫn phải thường xuyên làm việc với ở cứng.
- Node stream ra đời sử dụng mô hình pipeline
	- gulp với mô hình task runner
	- Các task được nối với nhau theo dạng ống. Điểm cuối của task này sẽ là điểm đầu của task kia. Việc này khá là hiệu quả khi quá trình làm việc chúng ta chỉ thao tác với ổ cứng lúc ban đầu lấy dữ liệu là sau khi kết thúc phiên làm việc của các task.
	- Vấn đề: Hạn chế một task chỉ làm ở mức đơn giản, vô tổ chức
	- ![enter image description here](https://uploads.toptal.io/blog/image/127175/toptal-blog-image-1536863567119-b0b5772060fae13b591a7fba7000b312.png)

- webpack:
	- JS bundler![enter image description here](https://uploads.toptal.io/blog/image/127176/toptal-blog-image-1536863593368-212994e174cd8e18b452d7fb700c3c61.png)
 
 - Nhược điểm khó kết hợp được webpack và jQuery
 
#### Link(s) to work:

### Day 17: 10/10/2018

#### Today's progress:
- Learn Gulp - Basic

#### Throught:
- What is a Build System?
- In modern front-end workflow, the build system works with 3 components −
    - Package managers
    - Preprocessors
    - Task runners and build tools
- **Package Managers**
    - It is used to automate the installation upgrade, removal of required dependencies, clean libraries, and packages used in the development environment. Example for package managers are bower and npm.
- **Preprocessors**
    - CSS − SASS, LESS and Stylus.
    - JS − CoffeeScript, LiveScript, TypeScript, etc.
    - HTML − Markdown, HAML, Slim, Jade, etc.
- **Task Runners**
    - Task runners automate tasks like SASS to CSS conversion, minify the files, optimize images, and many other tasks used in the development workflow. Gulp is one of the task runner in the modern front-end work environment and it runs on Node.
- **Setting Up Your Project**
    - Src − Location of pre-processed HTML source files and folders.
        - Images − Contains images which are uncompressed.
        - Scripts − Contains multiple pre-processed script files.
        - Styles − Contains multiple pre-processed CSS files.
    - Build − This folder will be created automatically which contains the production files.
        - Images − Contains compressed images.
        - Scripts − Single script file that contains minified codes.
        - Styles − Single CSS file that contains minified codes.
    - gulpfile.js − It is the configuration file, which is used to define our tasks.

#### Link(s) to work:

### Day 18: 18/1/2019

#### Today's progress

- Contribute to open source projects

#### Throught

- Learn --> Problem Solving
- Gain  --> Problem Resolved

#### Link(s) to work
