<h1>Section 1</h1>

**Overview**
- Create folder structure for project

**Details**
- Folder structure:
    - buid
    - config
    - source
        - assets
            - fonts
            - imgs
        - scripts
        - styles
            - components // composed of many small components
            - modules // Header, Footer
            - pages
            - settings // fonts, colors, variables, animation, typography
            - style.less
            - layouts
            - overrides // third-party libary: Bootstrap, SemanticUI
        - templates
            - layouts // base-1, base-2
            - pages // home, landing page, dashboard
            - modules // header, footer
            - Use swig to split html files into small modules
        - vendors

- webpack hash filename avoids cache
- in project use yarn, npm, bower to manager package
- Bootstrap 3 and jQuery

**Bookmark**

- [swig](https://github.com/paularmstrong/swig)
- [bower](https://bower.io/)

<h1>Section 2</h1>

**Overview**
- Naming convention for Class and Id 
- When to use margin or padding
- Some rules work when covert PSD to HTML CSS

**Details**

- Naming convention
    - Class: (-) Semantic
    - Id: Quick trace, JS
        - (_), using double underscore (__) when next level element
        - example: section_1__text

- Using margin between two elements of the same level
- Padding between container and content
- Some rules:
    - Mapping 1:1 between PSD and HTML, CSS
    - Make from top to bottom
    - No resize PSD
    - Not ebbreviated

**Bookmark**

- None

<h1>Contribute to open source projects</h1>

**Overview**
- How to contribute open source projects?

**Details**
- `Purpose`
  1. Learning --> Problem solving
    - Known issues
    - Fix issues
    - Referrent pull request
    - Use third party services/ tools
    - Learn how the integrate bots, CI, chatbots
  2. Gain --> Problem resolved

- `How?`
  1. Fork:
     Purpose: create a copy project
                                |
                `pull request`    |
     Project <--------------- branch
  2. Setup dev environment
    - Check out/ Clone
    - Install prerequisites
    - runable
    - how to test?
  3. Code on a branch and push
  4. Read/create issue
  5. Create pull request --> original repo
  6. Wait for review

   if the pull request Merged! --> Problem resolved
   else it Closed!
   if will contribute --> Step 4
   else --> Step 1

- `Which projects?`
  - Projects used ago
    - Interested
    - Phone number validation
    - Typos
    - Examples
    - Tests
  - Check `package.json` and `bower.json` of projects to find out what libraries the are using
   
**Bookmark**